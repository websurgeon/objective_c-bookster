//
//  BookDetailsVCTests.m
//  BooksterTests
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "BookDetailsVC.h"
#import "Book.h"
#import "Author.h"

#define HC_SHORTHAND
#import <OCHamcrestIOS/OCHamcrestIOS.h>

#define MOCKITO_SHORTHAND
#import <OCMockitoIOS/OCMockitoIOS.h>

#define TESTVALUE_BOOK_ISBN @"TESTVALUE_BOOK_ISBN"
#define TESTVALUE_BOOK_TITLE @"TESTVALUE_BOOK_TITLE"
#define TESTVALUE_AUTHOR_NAME @"TESTVALUE_AUTHOR_NAME"

@interface BookDetailsVCTests : XCTestCase

@property (strong, nonatomic) BookDetailsVC *sut;
@property (strong, nonatomic) Book *mockBook;
@property (strong, nonatomic) Author *mockAuthor;

@end

@implementation BookDetailsVCTests

@synthesize mockAuthor = _mockAuthor;

- (Author *)mockAuthor
{
    if (!_mockAuthor) {
        _mockAuthor = mock([Author class]);
        [given([_mockAuthor name]) willReturn:TESTVALUE_AUTHOR_NAME];
    }
    return _mockAuthor;
}

@synthesize mockBook = _mockBook;

- (Book *)mockBook
{
    if (!_mockBook) {
        _mockBook = mock([Book class]);
        [given([_mockBook isbn]) willReturn:TESTVALUE_BOOK_ISBN];
        [given([_mockBook title]) willReturn:TESTVALUE_BOOK_TITLE];
        [given([_mockBook authors]) willReturn:@[self.mockAuthor]];
    }
    return _mockBook;
}

- (void)setUp
{
    [super setUp];

    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.sut = [[BookDetailsVC alloc] init];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.sut = nil;
    self.mockAuthor = nil;
    self.mockBook = nil;
    [super tearDown];
}

- (void)testISBNLabelIsUpdatedWhenBookPropertyIsSet
{
    UILabel *mockLabel = mock([UILabel class]);
    [self.sut setBookISBN:mockLabel];
    [self.sut setBook:self.mockBook];
    [verify(mockLabel) setText:TESTVALUE_BOOK_ISBN];
}

- (void)testTitleLabelIsUpdatedWhenBookPropertyIsSet
{
    UILabel *mockLabel = mock([UILabel class]);
    [self.sut setBookTitle:mockLabel];
    [self.sut setBook:self.mockBook];
    [verify(mockLabel) setText:TESTVALUE_BOOK_TITLE];
}


- (void)testAuthor1NameLabelIsUpdatedWhenBookPropertyIsSet
{
    UILabel *mockLabel = mock([UILabel class]);
    [self.sut setBookAuthor1:mockLabel];
    [self.sut setBook:self.mockBook];
    [verify(mockLabel) setText:TESTVALUE_AUTHOR_NAME];
}


- (void)testISBNLabelIsClearedWhenBookPropertyIsSetNil
{
    UILabel *mockLabel = mock([UILabel class]);
    [self.sut setBookISBN:mockLabel];
    [self.sut setBook:nil];
    [verify(mockLabel) setText:@""];
}

- (void)testTitleLabelIsClearedWhenBookPropertyIsSetNil
{
    UILabel *mockLabel = mock([UILabel class]);
    [self.sut setBookTitle:mockLabel];
    [self.sut setBook:nil];
    [verify(mockLabel) setText:@""];
}

- (void)testAuthor1NameLabelIsClearedWhenBookPropertyIsSetNil
{
    UILabel *mockLabel = mock([UILabel class]);
    [self.sut setBookAuthor1:mockLabel];
    [self.sut setBook:nil];
    [verify(mockLabel) setText:@""];
}


@end
