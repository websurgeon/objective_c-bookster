//
//  BookFinderVCTests.m
//  BooksterTests
//
//  Created by Pete on 16/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "BookFinderVC.h"
#import "Book.h"
#import "Author.h"
#import "PBURLProtocol.h"
#import "APIOpenLibrary+MockConnections.h"

#define HC_SHORTHAND
#import <OCHamcrestIOS/OCHamcrestIOS.h>

#define MOCKITO_SHORTHAND
#import <OCMockitoIOS/OCMockitoIOS.h>

@interface BookFinderVCTests : XCTestCase

@end

@implementation BookFinderVCTests

- (void)setUp
{
    [super setUp];

    // Put setup code here. This method is called before the invocation of each test method in the class.
    [PBURLProtocol turnOn]; // register PBURLProtocol to receive connection requests before NSURLProtocol
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [PBURLProtocol resetHandlers];
    [PBURLProtocol turnOff];
    [super tearDown];
}

- (void)testDoSearch
{
    BOOL __block validConnection = NO;
    
    [PBURLProtocol responseDataHandler:^NSData *(NSURLRequest *request) {
        validConnection = [APIOpenLibrary shouldHandleURL:request];
        [self unblock];
        return nil;
    }];
    
    BookFinderVC *sut = [[BookFinderVC alloc] init];
    [sut doSearch:self];
    [self blockForAsync];
    
    assertThatBool(validConnection, equalToBool(YES));
}

#pragma mark - Async Test  Blocking / Unblocking

// define static (shared) condition object that can be used to block a thread until code completes on another thread
static NSCondition *_mainThreadCondition = nil;

- (void)blockForAsync
{
    if (!_mainThreadCondition) {
        _mainThreadCondition = [[NSCondition alloc] init];
    }
    // lock the condition so that wait can be performed
    [_mainThreadCondition lock];
    
    // block the thread by calling wait on locked condition
    [_mainThreadCondition wait];
    
    // THREAD IS BLOCKED UNTIL [_mainThreadCondition signal]
    
    // once thread is no longer blocked we can tidy up (by unlcoking and releasig the condition object)
    [_mainThreadCondition unlock];
    _mainThreadCondition = nil;
}

- (void)unblock
{
    // stop blocking the main thread by signaling the condition object
    [_mainThreadCondition signal];
}

@end
