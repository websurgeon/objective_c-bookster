//
//  PBURLProtocol.m
//  MockRequestHandling
//
//  Created by Peter on 10/07/2013.
//  Copyright (c) 2013 Coins Mobile. All rights reserved.
//

#import "PBURLProtocol.h"

@implementation PBURLProtocol

static PBMockShouldHandleRequest _shouldHandleRequestHandler;
static PBMockResponseDataForRequest _responseDataHandler;
static PBMockResponseStatusCodeForRequest _responseStatusCodeHandler;
static PBMockResponseHeadersForRequest _responseHeadersHandler;

# pragma mark - Superclass methods (overrides)

+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
    return [PBURLProtocol shouldHandleRequest:request];
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request
{
    return request;
}

- (void)startLoading {
    NSURLRequest *request = [self request];
    id client = [self client];
    
    NSData *data = [PBURLProtocol responseDataForRequest:request];
    NSError *error = nil;
    if(data) {
        // Send the canned data
        
        NSHTTPURLResponse *response = [[NSHTTPURLResponse alloc] initWithURL:[request URL] statusCode:[PBURLProtocol responseStatusCodeForRequest:request] HTTPVersion:@"HTTP/1.1" headerFields:[PBURLProtocol responseHeaderFieldsForRequest:request]];
        
        [client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
        [client URLProtocol:self didLoadData:data];
        [client URLProtocolDidFinishLoading:self];
    }
    else if(error) {
        // Send the canned error
        [client URLProtocol:self didFailWithError:error];
    }
}

- (void)stopLoading
{
    // no need to do anything, just implementing as it is a required method
}

#pragma mark - Private Methods

#pragma mark class methods for getting response values

+ (BOOL)shouldHandleRequest:(NSURLRequest *)request
{
    if (_shouldHandleRequestHandler) {
        return _shouldHandleRequestHandler(request);
    }
    return YES;
}

+ (NSData *)responseDataForRequest:(NSURLRequest *)request
{
    NSData *data = [NSData data];
    if (_responseDataHandler) {
        data = _responseDataHandler(request);
    }
    return data;
}

+ (NSInteger)responseStatusCodeForRequest:(NSURLRequest *)request
{
    NSInteger statusCode = 200;
    if (_responseStatusCodeHandler) {
        statusCode = _responseStatusCodeHandler(request);
    }
    return statusCode;
}

+ (NSDictionary *)responseHeaderFieldsForRequest:(NSURLRequest *)request
{
    NSDictionary *headers = @{};
    if (_responseHeadersHandler) {
        headers = _responseHeadersHandler(request);
    }
    return headers;
}

#pragma mark - Public Methods

#pragma mark toggle response mocking

+ (void)turnOn
{
    [NSURLProtocol registerClass:[PBURLProtocol class]];
}

+ (void)turnOff
{
    [NSURLProtocol unregisterClass:[PBURLProtocol class]];
}

+ (void)resetHandlers
{
    [PBURLProtocol shouldHandleRequestHandler:nil];
    [PBURLProtocol responseDataHandler:nil];
    [PBURLProtocol responseStatusCodeHandler:nil];
    [PBURLProtocol responseHeaderHandler:nil];
}


#pragma mark setter methods for response handlers

+ (void)shouldHandleRequestHandler:(PBMockShouldHandleRequest)shouldHandleRequestHandler
{
    _shouldHandleRequestHandler = shouldHandleRequestHandler;
}

+ (void)responseDataHandler:(PBMockResponseDataForRequest)responseDataHandler
{
    _responseDataHandler = responseDataHandler;
}

+ (void)responseStatusCodeHandler:(PBMockResponseStatusCodeForRequest)responseStatusCodeHandler
{
    _responseStatusCodeHandler = responseStatusCodeHandler;
}

+ (void)responseHeaderHandler:(PBMockResponseHeadersForRequest)responseHeaderHandler;
{
    _responseHeadersHandler = responseHeaderHandler;
}

@end
