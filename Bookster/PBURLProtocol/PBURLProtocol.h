//
//  PBURLProtocol.h
//  MockRequestHandling
//
//  Created by Peter on 10/07/2013.
//  Copyright (c) 2013 Coins Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBURLProtocol : NSURLProtocol

typedef BOOL (^PBMockShouldHandleRequest)(NSURLRequest *);
typedef NSData *(^PBMockResponseDataForRequest)(NSURLRequest *);
typedef NSInteger (^PBMockResponseStatusCodeForRequest)(NSURLRequest *);
typedef NSDictionary *(^PBMockResponseHeadersForRequest)(NSURLRequest *);

+ (void)turnOn;

+ (void)turnOff;

// set block that checks if request should be handled by this mock protocol
+ (void)shouldHandleRequestHandler:(PBMockShouldHandleRequest)shouldHandleRequestHandler;

// set block of code to return response data for the supplied request
+ (void)responseDataHandler:(PBMockResponseDataForRequest)responseDataHandler;

// set block of code to return response status code for the supplied request
+ (void)responseStatusCodeHandler:(PBMockResponseStatusCodeForRequest)responseStatusCodeHandler;

// set block of code to return http response headers for the supplied request
+ (void)responseHeaderHandler:(PBMockResponseHeadersForRequest)responseHeaderHandler;

// reset all handlers to nil
+ (void)resetHandlers;

@end
