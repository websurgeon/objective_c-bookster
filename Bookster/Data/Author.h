//
//  Author.h
//  Bookster
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Author : NSObject

@property (strong, nonatomic) NSString *name;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)dictionaryValue;

- (NSString *)jsonValue;

@end
