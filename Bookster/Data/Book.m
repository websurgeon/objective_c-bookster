//
//  Book.m
//  Bookster
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "Book.h"
#import "Author.h"
#import "APIOpenLibrary.h"

@implementation Book

#pragma mark - Public Properties

@synthesize isbn = _isbn;
@synthesize title = _title;
@synthesize authors = _authors;
@synthesize coverImageLarge = _coverImageLarge;

#pragma mark - Public Class Methods

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    if (self && dictionary) {
        self->_isbn = [dictionary objectForKey:@"isbn"];
        self->_title = [dictionary objectForKey:@"title"];
        NSArray *authorsArr = [dictionary objectForKey:@"authors"];
        NSMutableArray *authors = [[NSMutableArray alloc] init];
        if ([authorsArr count] > 0) {
            for (NSDictionary *authorDict in authorsArr) {
                Author *author = [[Author alloc] initWithDictionary:authorDict];
                if (author) {
                    [authors addObject:author];
                }
            }
            self->_authors = authors;
        }
    }
    return self;
}

- (id)initWithJSONData:(NSData *)jsonData
{
    NSError *jsonError = nil;
    NSDictionary *jsonDict = nil;
    @try {
        jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData
                                                   options:kNilOptions
                                                     error:&jsonError];        
    }
    @catch(NSException *jsonEx) {
        // TODO: use delegate to handle exceptions
        NSLog(@"JSON Exception: %@", jsonEx);
    }
    self = [self initWithDictionary:jsonDict];
    return self;
}

- (NSDictionary *)dictionaryValue
{
    NSMutableDictionary *requestDict = [NSMutableDictionary dictionary];
    
    [requestDict setValue:self.isbn forKey:@"isbn"];
    [requestDict setValue:self.title forKey:@"title"];
    if ([self.authors count] > 0) {
        NSMutableArray *authors = [NSMutableArray array];
        for (Author *author in self.authors) {
            [authors addObject:[author dictionaryValue]];
        }
        [requestDict setValue:authors forKey:@"authors"];
    }
    return [NSDictionary dictionaryWithDictionary:requestDict];
}

- (NSString *)jsonValue
{
    NSString *jsonStr = nil;
    NSError *jsonError = nil;
    @try {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self dictionaryValue] options:kNilOptions error:&jsonError];
        if (jsonData) {
            jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        } else {
//            TODO: handle errors via delegate
            NSLog(@"TODO: handle errors via delegate: %@", [jsonError localizedDescription]);
        }
    }
    @catch(NSException *ex) {
//        TODO: handle exceptions via delegate
        NSLog(@"TODO: handle exceptions via delegate: %@", [ex description]);
    }
    return jsonStr;
}

+ (void)fetchBookByISBN:(NSString *)isbn responseBlock:(void (^)(Book *book, NSError *error))responseBlock
{
    [APIOpenLibrary fetchBookByISBN:isbn responseBlock:responseBlock];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Book [%@]: %@", self.isbn, self.title];
}

+ (id)demoBook1
{
    return [APIOpenLibrary demoBook1];
}

@end
