//
//  Book.h
//  Bookster
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Book : NSObject

@property (strong, nonatomic) NSString *isbn;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSArray *authors;
@property (strong, nonatomic) UIImage *coverImageLarge;

- (id)initWithDictionary:(NSDictionary *)dictionary;

- (id)initWithJSONData:(NSData *)jsonData;

- (NSDictionary *)dictionaryValue;

- (NSString *)jsonValue;

+ (void)fetchBookByISBN:(NSString *)isbn responseBlock:(void (^)(Book *book, NSError *error))responseBlock;

+ (id)demoBook1;

@end
