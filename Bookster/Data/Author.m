//
//  Author.m
//  Bookster
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "Author.h"

@implementation Author

#pragma mark - Public Properties

@synthesize name = _name;

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    self->_name = [dictionary objectForKey:@"name"];
    return self;
}

- (NSDictionary *)dictionaryValue
{
    NSMutableDictionary *requestDict = [NSMutableDictionary dictionary];
    
    [requestDict setValue:self.name forKey:@"name"];

    return [NSDictionary dictionaryWithDictionary:requestDict];
}

- (NSString *)jsonValue
{
    NSString *jsonStr = nil;
    NSError *jsonError = nil;
    @try {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self dictionaryValue] options:kNilOptions error:&jsonError];
        if (jsonData) {
            jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        } else {
            //            TODO: handle errors via delegate
            NSLog(@"TODO: handle errors via delegate: %@", [jsonError localizedDescription]);
        }
    }
    @catch(NSException *ex) {
        //        TODO: handle exceptions via delegate
        NSLog(@"TODO: handle exceptions via delegate: %@", [ex description]);
    }
    return jsonStr;
}

@end
