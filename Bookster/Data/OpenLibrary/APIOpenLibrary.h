//
//  APIOpenLibrary.h
//  Bookster
//
//  Created by Pete on 14/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Book;

@interface APIOpenLibrary : NSObject

+ (void)fetchBookByISBN:(NSString *)isbn responseBlock:(void (^)(Book *book, NSError *error))responseBlock;

// TODO: consider moving the following methods to a category

// Some methods for testing...

+ (NSString *)isbnFromBooksURL:(NSURL *)url;

+ (NSString *)getISBNFromKey:(NSString *)key;

+ (NSString *)demoBook1ResponseJSONData;

+ (NSString *)demoBook2ResponseJSONData;

+ (Book *)demoBook1;

+ (Book *)demoBook2;

@end
