//
//  APIOpenLibrary.m
//  Bookster
//
//  Created by Pete on 14/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "APIOpenLibrary.h"
#import "Book.h"
#import "Author.h"

@implementation APIOpenLibrary

+ (void)fetchBookByISBN:(NSString *)isbn responseBlock:(void (^)(Book *book, NSError *error))responseBlock
{
    NSString *isbnRequestString = [NSString stringWithFormat:@"https://openlibrary.org/api/books?bibkeys=ISBN:%@&format=json&jscmd=data", isbn];
    
    NSURL *url = [NSURL URLWithString:isbnRequestString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        Book *book = nil;
        if (connectionError == nil) {
            book = [[self booksFromResponseData:data] lastObject];
        }

        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            responseBlock(book, connectionError);
        }];
    }];
}

+ (NSArray *)booksFromResponseData:(NSData *)data
{
    NSError *jsonError = nil;
    NSDictionary *jsonDict = nil;
    @try {
        jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                   options:kNilOptions
                                                     error:&jsonError];        
    }
    @catch(NSException *jsonEx) {
        // TODO: use delegate to handle exceptions
        NSLog(@"JSON Exception: %@", jsonEx);
    }
    
    NSMutableArray *books = [NSMutableArray array];
    
    NSArray *ISBNs = [jsonDict allKeys];
    
    for (NSString *bookKey in ISBNs) {
        NSLog(@"found bookKey: %@", bookKey);
        NSString *isbn = [self getISBNFromKey:bookKey];
        if (isbn != nil) {
            Book *book = [[Book alloc] init];
            NSLog(@"creating book for: %@", isbn);
            
            book.isbn = isbn;
            
            NSDictionary *bookDetail = [jsonDict objectForKey:bookKey];
            book.title = [bookDetail objectForKey:@"title"];
            
            NSArray *authorsArray = [bookDetail objectForKey:@"authors"];
            NSMutableArray *authors = [NSMutableArray array];
            for (NSDictionary *authorData in authorsArray) {
                [authors addObject:[[Author alloc] initWithDictionary:authorData]];
            }
            book.authors = [NSArray arrayWithArray:authors];
            NSDictionary *coverImageURLs = [bookDetail objectForKey:@"cover"];
            
            if ([coverImageURLs objectForKey:@"large"]) {
                NSLog(@"FETCH IMAGE: %@",[NSURL URLWithString:[coverImageURLs objectForKey:@"large"]]);
                // TODO: fetch image in background thread (maybe lazy load as part of book object instead?)
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[coverImageURLs objectForKey:@"large"]]];
                book.coverImageLarge = [UIImage imageWithData:imageData];
            }
            [books addObject:book];
        }
    }
    
    return [NSArray arrayWithArray:books];
}

+ (NSString *)getISBNFromKey:(NSString *)key
{
    NSString *pattern = @"ISBN:(\\d*)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:0 error:NULL];

    NSTextCheckingResult *match = [regex firstMatchInString:key options:0 range:NSMakeRange(0, [key length])];
    NSString *isbn = nil;
    if (match != nil) {
        isbn = [key substringWithRange:[match rangeAtIndex:1]];
    }
    return isbn;
}

#pragma mark - Testing methods

+ (NSString *)demoBook1ResponseJSONData
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"isbn0201558025" ofType:@"json"];
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return text;
}

+ (NSString *)demoBook2ResponseJSONData
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"isbn0385472579" ofType:@"json"];
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return text;
}

+ (Book *)demoBook1
{
    NSData *data = [[self demoBook1ResponseJSONData] dataUsingEncoding:NSUTF8StringEncoding];
    return [[self booksFromResponseData:data] lastObject];
}

+ (Book *)demoBook2
{
    NSData *data = [[self demoBook2ResponseJSONData] dataUsingEncoding:NSUTF8StringEncoding];
    return [[self booksFromResponseData:data] lastObject];
}

+ (NSString *)isbnFromBooksURL:(NSURL *)url
{
    
    //https://openlibrary.org/api/books?bibkeys=ISBN:0385472579&format=json&jscmd=data
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [url.query componentsSeparatedByString:@"&"]) {
        NSArray *keyValue = [param componentsSeparatedByString:@"="];
        if([keyValue count] < 2) continue;
        [params setObject:[keyValue objectAtIndex:1] forKey:[keyValue objectAtIndex:0]];
    }
    return [self getISBNFromKey:[params objectForKey:@"bibkeys"]];
}

@end
