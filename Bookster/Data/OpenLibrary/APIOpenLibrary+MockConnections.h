//
//  APIOpenLibrary+MockConnections.h
//  Bookster
//
//  Created by Pete on 16/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "APIOpenLibrary.h"

@interface APIOpenLibrary (MockConnections)

// PBURLProtocol methods for overriding url requests when testing

+ (BOOL)shouldHandleURL:(NSURLRequest *)request;

+ (NSData *)responseDataHandler:(NSURLRequest *)request;

@end
