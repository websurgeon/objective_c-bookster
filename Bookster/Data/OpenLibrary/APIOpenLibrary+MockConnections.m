//
//  APIOpenLibrary+MockConnections.m
//  Bookster
//
//  Created by Pete on 16/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "APIOpenLibrary+MockConnections.h"

@implementation APIOpenLibrary (MockConnections)

+ (BOOL)shouldHandleURL:(NSURLRequest *)request
{    
    NSString *urlString = [[request URL] absoluteString];
    NSLog(@"URL Requested: %@", urlString);
    if (([request.URL.host isEqualToString:@"openlibrary.org"] ||
         [request.URL.host isEqualToString:@"covers.openlibrary.org"])) {
        return YES;
    } else {
        return NO;
    }
}

+ (NSData *)responseDataHandler:(NSURLRequest *)request
{
//    NSLog(@"URL Requested: %@", [[request URL] absoluteString]);
//    NSLog(@"URL host: %@", [[request URL] host]);
    
    if ([request.URL.host isEqualToString:@"openlibrary.org"] &&
        [request.URL.path isEqualToString:@"/api/books"]) {
        NSString *isbn = [APIOpenLibrary isbnFromBooksURL:request.URL];
        if ([isbn isEqualToString:@"1"]) {
            return [[self demoBook1ResponseJSONData] dataUsingEncoding:NSUTF8StringEncoding
                    ];            
        } else if ([isbn isEqualToString:@"2"]) {
            return [[self demoBook2ResponseJSONData] dataUsingEncoding:NSUTF8StringEncoding
                    ];
        }
    } else if ([request.URL.host isEqualToString:@"covers.openlibrary.org"]) {
        NSString *imagePath = filePathForCoverImageURL(request.URL);
//        NSLog(@"Image Path: %@",filePathForCoverImageURL(request.URL));
        if (imagePath) {
            return [NSData dataWithContentsOfFile:imagePath];
        }
    }
    return [@"" dataUsingEncoding:NSUTF8StringEncoding];;
}

NSString *filePathForCoverImageURL(NSURL *url) {
    // e.g. https://covers.openlibrary.org/b/id/135182-L.jpg
    
    NSString *fileName = [[[url absoluteString] pathComponents] lastObject];
    return [[NSBundle mainBundle] pathForResource:[fileName stringByDeletingPathExtension] ofType:[[url absoluteString] pathExtension]];
}

@end
