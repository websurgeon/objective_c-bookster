//
//  BookScanVC.m
//  Bookster
//
//  Created by Pete on 01/12/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "BookScanVC.h"
#import <AVFoundation/AVFoundation.h>


@interface BookScanVC () <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) AVCaptureDevice* device;
@property (strong, nonatomic) AVCaptureDeviceInput* input;
@property (strong, nonatomic) AVCaptureMetadataOutput* output;
@property (strong, nonatomic) AVCaptureSession* session;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@end

@implementation BookScanVC

@synthesize preview = _preview;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startScan];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)startScan
{
    NSError *error = nil;
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device
                                                                        error:&error];
    
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    if (input) {
        [session addInput:input];
    } else {
        NSLog(@"Error: %@", error);
    }
    [session addOutput:output];
    
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeEAN13Code]];
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    
    
    // Preview
    AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.frame = CGRectMake(0, 0, self.preview.frame.size.width, self.preview.frame.size.height);
    [self.preview.layer insertSublayer:previewLayer atIndex:0];
    
    [session startRunning];

    self.device = device;
    self.session = session;
    self.input = input;
    self.output = output;
    self.previewLayer = previewLayer;

}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSString *isbnCode = nil;
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeEAN13Code]) {
            isbnCode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            if (isbnCode) {
                NSLog(@"ISBN Code: %@", isbnCode);
                if ([self.delegate respondsToSelector:@selector(scanDidFindISBN:)]) {
                    [self.delegate scanDidFindISBN:isbnCode];
                };
                [self performSegueWithIdentifier:@"unwindToBookFinderFromBookScan" sender:metadata];
                [self.session stopRunning];
            }
            break;
        }
    }
    
}

@end
