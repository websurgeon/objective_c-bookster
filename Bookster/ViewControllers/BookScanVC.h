//
//  BookScanVC.h
//  Bookster
//
//  Created by Pete on 01/12/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BookScanProtocol <NSObject>

@optional

- (void)scanDidFindISBN:(NSString *)isbn;

@end

@interface BookScanVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *preview;
@property (assign, nonatomic) id <BookScanProtocol> delegate;

@end
