//
//  BookFinderVC.m
//  Bookster
//
//  Created by Pete on 14/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "BookFinderVC.h"
#import "Book.h"
#import "BookDetailsVC.h"
#import "BookScanVC.h"

@interface BookFinderVC () <BookScanProtocol>

@end

@implementation BookFinderVC

@synthesize btn_search = _btn_search;

-(void)setTxt_isbn:(UITextField *)txt_isbn
{
    _txt_isbn = txt_isbn;
    if (_txt_isbn) {
        [_txt_isbn setText:self.isbn];
    }
}

@synthesize txt_isbn = _txt_isbn;

- (UITextField *)txt_isbn
{
    if (!_txt_isbn) {
        // need to create a text field as it is used to store isbn value
        UITextField *txtF = [[UITextField alloc] init];
        _txt_isbn = txtF;
    }
    return _txt_isbn;
}

// Using txt_isbn to store isbn text value so am overriding getter and setter
// @synthesize isbn = _isbn;

- (NSString *)isbn
{
    return self.txt_isbn.text;
}

- (void)setIsbn:(NSString *)isbn
{
    [self.txt_isbn setText:isbn];
}

@synthesize activityIndicator = _activityIndicator;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doSearch:(id)sender
{
    [self.activityIndicator startAnimating];
    [self requestBookByISBN:self.isbn responseBlock:^(Book *book, NSError *error) {
        [self.activityIndicator stopAnimating];
        NSLog(@"BOOK JSON: %@", [book jsonValue]);
        [self performSegueWithIdentifier:@"segue_BookFinderVC_to_BookDetailsVC" sender:book];
    }];
}

- (void)requestBookByISBN:(NSString *)isbn responseBlock:(void (^)(Book *book, NSError *error))responseBlock
{
    [Book fetchBookByISBN:isbn responseBlock:responseBlock];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segue_BookFinderVC_to_BookDetailsVC"]) {
        BookDetailsVC *vc = (BookDetailsVC *)segue.destinationViewController;
        vc.book = (Book *)sender;
    } else if ([[segue identifier] isEqualToString:@"segue_BookFinderVC_to_BookScanVC"]) {
        BookScanVC *vc = (BookScanVC *)segue.destinationViewController;
        vc.delegate = self;
    }
}

- (IBAction)unwindSegueToBookFinder:(UIStoryboardSegue *)segue
{
    
}

- (void)scanDidFindISBN:(NSString *)isbn
{
    self.txt_isbn.text = isbn;
}

@end
