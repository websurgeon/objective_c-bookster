//
//  BookDetailsVC.m
//  Bookster
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "BookDetailsVC.h"
#import "Book.h"
#import "Author.h"


@interface BookDetailsVC ()

@end

@implementation BookDetailsVC

@synthesize book = _book;

- (void)setBook:(Book *)book
{
    _book = book;
    if (_book) {
        [self updateDisplay];
    } else {
        [self updateDisplayForNoBook];
    }
}

@synthesize bookCoverImageView = _bookCoverImageView;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    if (self.book) {
        [self updateDisplay];
    } else {
        [self updateDisplayForNoBook];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
//    [self performSelector:@selector(loadDemoData) withObject:Nil afterDelay:3.0];
}

- (void)loadDemoData
{
    [self setBook:[Book demoBook1]];
}

- (void)updateDisplay
{
    [self.bookISBN setText:self.book.isbn];
    [self.bookTitle setText:self.book.title];
    Author *author1 = [self.book.authors lastObject];
    if (author1) {
        [self.bookAuthor1 setText:author1.name];
    } else {
        [self.bookAuthor1 setText:@"?"];
    }
    if (self.bookCoverImageView && self.book.coverImageLarge) {
        [self.bookCoverImageView setImage:self.book.coverImageLarge];
    }
}

- (void)updateDisplayForLoadingState
{
    [self.bookISBN setText:@"..."];
    [self.bookTitle setText:@"..."];
    [self.bookAuthor1 setText:@"..."];
    [self.bookCoverImageView setImage:nil];
}

- (void)updateDisplayForNoBook
{
    [self.bookISBN setText:@""];
    [self.bookTitle setText:@""];
    [self.bookAuthor1 setText:@""];
    [self.bookCoverImageView setImage:nil];
}


@end
