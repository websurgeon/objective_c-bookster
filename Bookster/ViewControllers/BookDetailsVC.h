//
//  BookDetailsVC.h
//  Bookster
//
//  Created by Pete on 12/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Book;

@interface BookDetailsVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *bookISBN;
@property (strong, nonatomic) IBOutlet UILabel *bookTitle;
@property (strong, nonatomic) IBOutlet UILabel *bookAuthor1;
@property (strong, nonatomic) IBOutlet UIImageView *bookCoverImageView;

@property (strong, nonatomic) Book *book;

@end
