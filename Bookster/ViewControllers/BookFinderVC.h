//
//  BookFinderVC.h
//  Bookster
//
//  Created by Pete on 14/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookFinderVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btn_search;
@property (weak, nonatomic) IBOutlet UITextField *txt_isbn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSString *isbn;

- (IBAction)doSearch:(id)sender;

@end
